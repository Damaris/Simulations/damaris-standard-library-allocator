#include <cstdlib>
#include <new>
#include <limits>
#include <sstream>
#include <iostream>
#include <vector>

#include <Damaris.h>

/**
* 
*  mpicxx -I ~/local/include -L ~/local/lib  -o test.exe main.cpp -ldamaris
* 
 Requires allocate.xml config in same directory as exe :
>cat allocate.xml
<?xml version="1.0"?>
<simulation name="2dmesh" language="c" xmlns="http://damaris.gforge.inria.fr/damaris/model">
    <architecture>
        <domains count="2" />
        <dedicated cores="1" nodes="0" />
        <placement />        
        <buffer name="damaris-buffer" size="536870912" />
        <queue  name="damaris-queue" size="100" />
    </architecture>
    <data>
        <parameter name="size"     type="int" value="4" comment="This will be set to the number of Damaris cients, so depends on mpirun -nm X value" />
        <parameter name="PRESSURE_param_0"   type="int" value="1"  />

        <layout name="cells"  type="int" dimensions="PRESSURE_param_0 " global="PRESSURE_param_0 * size" />

        <variable name="PRESSURE" type="scalar" layout="cells" mesh="mesh" centering="nodal" store="MyStore" />
    </data>
    <storage>
        <store name="MyStore" type="HDF5">
            <option key="FileMode">FilePerCore</option>
            <option key="XDMFMode">NoIteration</option>
            <option key="FilesPath"></option>
        </store>
    </storage>
    <actions>
    </actions>
    <log FileName="log/2dmesh" RotationSize="5" LogFormat="[%TimeStamp%]: %Message%" LogLevel="info" Flush="false" />
</simulation>
*/


/**
This is an simple stallocator that can be used to compare against the Mallocator Damaris allocator below.
*/
template<class T>
struct Sallocator
{
    typedef T value_type;
 
    Sallocator () = default;
 
    template<class U>
    constexpr Sallocator (const Sallocator <U>&) noexcept {}
 
    [[nodiscard]] T* allocate(std::size_t n)
    {
        if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
            throw std::bad_array_new_length();
 
        if (auto p = static_cast<T*>(std::malloc(n * sizeof(T))))
        {
            report(p, n);
            return p;
        }
 
        throw std::bad_alloc();
    }
 
    void deallocate(T* p, std::size_t n) noexcept
    {
        report(p, n, 0);
        std::free(p);
    }
private:
    void report(T* p, std::size_t n, bool alloc = true) const
    {
        std::cout << (alloc ? "Alloc: " : "Dealloc: ") << sizeof(T) * n
                  << " bytes at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(p) << std::dec << '\n';
    }
};
 
template<class T, class U>
bool operator==(const Sallocator <T>&, const Sallocator <U>&) { return true; }
 
template<class T, class U>
bool operator!=(const Sallocator <T>&, const Sallocator <U>&) { return false; }
 
 
 
 
/**
This the double buffered (by Dmaris 'blocks' Damaris memory allocator
*/
template<class T>
struct Mallocator
{
    typedef T value_type;
    Mallocator () = delete ;  // Was default
    
    
    Mallocator (std::string damaris_variable) 
    : damaris_variable_str_(damaris_variable) 
    {
        active_block_ = 1         ; // this will be swapped to 0 on first allocation
        paramaters_set_ = false   ;
        has_error_      = false   ;
        current_size_bytes_ = 0   ;
        data_ptr_0_     = nullptr ;
        data_ptr_1_     = nullptr ;
        rank_ = 0 ;  
        this->damaris_param_str_ = damaris_variable + "_param_" + "0" ;
    }
    
    Mallocator (std::string damaris_variable, int rank) 
    : damaris_variable_str_(damaris_variable),
      rank_(rank)
    {
        active_block_ = 1         ;  // this will be swapped to 0 on first allocation
        paramaters_set_ = false   ;
        has_error_      = false   ;
        current_size_bytes_ = 0   ;
        data_ptr_0_     = nullptr ;
        data_ptr_1_     = nullptr ;
        this->damaris_param_str_ = damaris_variable + "_param_" + "0" ;
    }
    
    ~Mallocator() {
        std::cout  << "INFO: active_block_ :" << active_block_ << "  Rank: " << rank_ << "  Mallocator Destructor Called: Variable named: " << damaris_variable_str_  <<  '\n';
        finalize( 0 ) ;
        finalize( 1 ) ;
    }
    
    void PrintError (std::string extra ) {
        std::cerr << "ERROR: Mallocator::allocate() active_block_ : " << active_block_ << "  Rank: " << rank_ << extra << " failed:   : " << damaris_error_string(damaris_err_) << "  Variable: " <<  damaris_variable_str_  <<  '\n';
    }
    
    template<class U>
    constexpr Mallocator (const Mallocator <U>&) noexcept {}
    
    std::string& GetName( void ) {
        return this->damaris_variable_str_ ;
    }

    [[nodiscard]] T* allocate(std::size_t n)
    {
        // As Damaris paramaters can only support int types, we
        // change the test for maximum size of request 
        if (n > std::numeric_limits<int>::max() )
            throw std::bad_array_new_length();
        
        int n_request = static_cast<int>(n) ;
        
        // Swap the block number. 
        // Typical use of allocator seems to be:
        // allocate space
        // we run out of space, allocate replacement, deallocate old space
        
        // This is the only place that we swap the buffers.
        (active_block_  == 0 ) ? active_block_ = 1  : active_block_ = 0  ;

        const bool ok = true ;
        if (ok != SetDamarisParameter( n_request ) ) {
            PrintError( " SetDamarisParameter::damaris_parameter_set() ") ;
            throw std::bad_alloc();
        }
        
        
        // pointer_set_ is set to true in SetDamarisParameter()
        if (ok != SetPointersToDamarisShmem( active_block_ ) ) {
           PrintError( " SetPointersToDamarisShmem::damaris_alloc_block() ") ;
            throw std::bad_alloc();
        }
        
        (active_block_  == 0 ) ? n_request_0_ = n_request  : n_request_1_ = n_request  ;
        
        current_size_bytes_ = n_request * sizeof(T) ;
        report(data_ptr( active_block_ ), n_request);
        
        // auto p = static_cast<T*>
        return static_cast<T*>( data_ptr( active_block_ ) ) ;

    }
    
     void deallocate(T* p, std::size_t n) noexcept
    {
        report(data_ptr( active_block_ ), n, 0);
        const bool ok = true ;
        
        // Don't deallocate anything, just use the other buffer 
        // N.B. buffer will be  swapped in the allocate() method, 
        // which (as tested) will be called before the previous memory is deallocated
        // (active_block_  == 0 ) ? active_block_ = 1  : active_block_ = 0  ;
        // std::free(p);
    }

    /**
    * This will confirm to Damaris the current memory region is available for server side 
    * processing and the client has finished writing/reading
    *
    */
    void finalize(int active_block ) 
    {
        int n_request ;
        (active_block  == 0 ) ? n_request = n_request_0_  : n_request = n_request_1_   ;
        report(data_ptr( active_block ), n_request, 0);
        
        
        const bool ok = true ;
        
        damaris_err_ = damaris_commit_block  (damaris_variable_str_.c_str(), active_block) ;
        if (damaris_err_ != DAMARIS_OK) {
             PrintError( " damaris_commit_block() ") ; 
        }
      
        damaris_err_ = damaris_clear_block(damaris_variable_str_.c_str(), active_block) ;
        if (damaris_err_ != DAMARIS_OK) {
            PrintError( " damaris_clear_block() ") ; 
        }
        (active_block  == 0 ) ? data_ptr_0_ = nullptr  : data_ptr_1_ = nullptr  ; 
       
    }
    
    int GetActiveBlock ( void ) {
        return (active_block_) ;
    }

private:
    void report(T* p, std::size_t n, bool alloc = true) const
    {
        std::cout << "INFO: active_block_ :" << active_block_ << "  Rank: " << rank_ << "  Variable named: " << damaris_variable_str_ << " " << (alloc ? "Alloc: " : "Dealloc: ") << sizeof(T) * n
                  << " bytes at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(p) << std::dec << '\n';
    }
    std::string damaris_variable_str_ ;
    std::string damaris_param_str_ ;
    // Two data pointers as we double buffer our memory regions and swap them when a allocator needs to deallocate
    T * data_ptr_0_ ;
    T * data_ptr_1_ ;
    int n_request_0_   ; 
    int n_request_1_   ; 
    bool has_error_ ;
    bool paramaters_set_ ;
    int  damaris_err_ ;              //!<  Set to != DAMARIS_OK if a Damaris error was returned by a Damaris API function call
    int rank_ ;
    int active_block_ ;               //!<  Used to set the active memory region that the allocator gives to the container. 0 or 1

    std::size_t current_size_bytes_ ;  //!<  
    
    T * data_ptr( int active_block ) 
    {
        if (active_block == 0 )
            return (data_ptr_0_) ;
        else 
            return (data_ptr_1_) ;
    }
            
            
    /**
    *  Method to set the Damaris paramater values.
    *
    *  /param [IN] paramSizeVal : A reference to a value to set the Damaris paramater value to.
    *
    *  /implicit                : Implicitly uses the array of paramater names: \ref param_names_
    */
    bool SetDamarisParameter( int paramSizeVal ) 
    {
        bool resbool = true ;
        
        damaris_err_ = damaris_parameter_set(damaris_param_str_.c_str(), &paramSizeVal, sizeof(int));
        if (damaris_err_ != DAMARIS_OK) {
            resbool = false ;
            has_error_ = true ;
        }
        
        if (resbool == true) 
            paramaters_set_ = true ;
        
        return resbool ;
    }
    

    /**
    *  Method to set the Damaris paramater values.
    *
    *  /implicit                : Implicitly uses the Damaris variable name string  \ref damaris_variable_str_
    *  /implicit                : Implicitly uses the class data element : \ref data_ptr_
    */
    bool SetPointersToDamarisShmem( int active_block )
    {
        bool resbool = true ;
        if (paramaters_set_ == true )
        {
            // Allocate memory in the shared memory section... 
            if ((active_block == 0) && (data_ptr_0_ == nullptr)) {
                damaris_err_ = damaris_alloc_block (damaris_variable_str_.c_str(), active_block, (void **) &data_ptr_0_) ;
            } else if ((active_block == 1) && (data_ptr_1_ == nullptr)) {
                damaris_err_ = damaris_alloc_block (damaris_variable_str_.c_str(), active_block, (void **) &data_ptr_1_) ;
            } else {
                damaris_err_ = DAMARIS_OK ;
            }
            
            if (damaris_err_ != DAMARIS_OK) {
                has_error_ = true ;
                resbool = false ;
            }
            // pointer_set_ = true ;
        } else {
            damaris_err_ = -1 ;
            has_error_ = true ;
            resbool = false ;
        }
        
        return resbool ;
    }
    
    
    /**
    *  Method to commit the memory of the data written to the Damaris variable - 
    *  Indicates that we will not write any more data to \ref data_ptr_
    *
    *  /implicit                : Implicitly uses the variable name string  \ref damaris_variable_str_
    */
    bool  CommitVariableDamarisShmem( int active_block )
    {
        bool resbool = true ;
        // Signal to Damaris we are done writing data to this block for this iteration
        damaris_err_ = damaris_commit_block  (damaris_variable_str_.c_str(), active_block) ;
        if (damaris_err_ != DAMARIS_OK) {
            has_error_ = true ;
            resbool = false  ;
        }
        return resbool ;
    }
    
    /**
    *  Method to release the memory of the data written to the Damaris variable - 
    *  Indicates that Damaris may take control of the shared memory area that was used for the variable \ref data_ptr_
    *
    *  /implicit                : Implicitly uses the variable name string  \ref damaris_variable_str_
    */
    bool ClearVariableDamarisShmem( int active_block )
    {
        bool resbool = true ;
        // Signal to Damaris it has complete charge of the memory area               
        damaris_err_ = damaris_clear_block(damaris_variable_str_.c_str(), active_block) ;
        if (damaris_err_ != DAMARIS_OK) {
            has_error_ = true ;
            resbool = false  ;
        }
        (active_block  == 0 ) ? data_ptr_0_ = nullptr  : data_ptr_1_ = nullptr  ; 
        
        return resbool ;
    }
};

template<typename T>
void printInfo(std::vector<T, Sallocator<int> >& svect) {
        std::cout << "svect.size() = "  << svect.size() << " units at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(svect.data()) << std::dec << '\n';
                  
}

class UseVect {
public:

    UseVect( int rank ) 
    : rank_(rank) 
    {
        // Mallocator<int> m_alloc_p(std::string("PRESSURE"), rank) ; 
        // Mallocator<int> * m_alloc_p = new Mallocator<int> (std::string("PRESSURE"),rank_) ;
        // p_s = new std::vector<int, Mallocator<int> >(*m_alloc_p) ;
        
        // p_s = new std::vector<int, Mallocator<int> >(std::move(* (new Mallocator<int> (std::string("PRESSURE"),rank_)))) ;
         p_s = new std::vector<int, Mallocator<int> >(Mallocator<int> (std::string("PRESSURE"),rank_)) ;

    } ;
    
    ~UseVect() {
        
        // delete m_alloc_p ; // this is released when p_s is deleted
        delete p_s ;
        
    } ;
   

    void printInfo( ) {
        /* std::cout << m_alloc_p->GetName() <<"  rank: " << rank_ << " p_s->size() = "  << p_s->size() << " units at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(p_s->data()) << std::dec << " Values: " ;*/
        std::cout << "PRINTINFO: " << "  rank: " << rank_ << " p_s->size() = "  << p_s->size() << " units at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(p_s->data()) << std::dec << " Values: " ;

        for (auto element : *p_s) {
            std::cout << element << " ";
        }
         std::cout  << '\n';
    }
   
    void TestAlloc( ) {
        std::cout << "INFO: " << rank_ << ": p_s->reserve(2)" << std::endl ;
        p_s->reserve(200) ; 
        
        p_s->push_back(42);
        printInfo() ;
        p_s->push_back(43);
        printInfo() ;
        p_s->push_back(44);
        printInfo() ;
        p_s->push_back(45);
        printInfo() ;
        
        std::cout << "INFO: " << rank_ << ": p_s->resize(8,0)" << std::endl ;
        p_s->resize(8, 0) ;
        printInfo() ;
        
        
        p_s->push_back(46);
        printInfo() ;
        p_s->push_back(47);
        printInfo() ;
        p_s->push_back(48);
        printInfo() ;
        p_s->push_back(49);
        printInfo() ;
        p_s->push_back(50);
        printInfo() ;
        
        std::cout << "INFO: " << rank_ << ": p_s->clear" << std::endl ;
        p_s->clear() ;
        printInfo() ;
         
        p_s->push_back(52);
        printInfo() ;
        p_s->push_back(53);
        printInfo() ;
        p_s->push_back(54);
        printInfo() ;
        
        std::cout << "INFO: " << rank_ << ": p_s->pop_back" << std::endl ;
        p_s->pop_back() ;
        printInfo() ;
        
        std::cout << "INFO: " << rank_ << ": p_s->reserve(20)" << std::endl ;
        p_s->reserve(20) ;
        printInfo() ;
        
        for (int i = 0 ; i < 30; i++) {
            p_s->push_back(55+i);
            printInfo() ;
        }
        
        std::cout << "INFO: " << rank_ << ": p_s->resize(12,0)" << std::endl ;
        p_s->resize(12, 0) ;
        printInfo() ;
         std::cout << "INFO: " << rank_ << ": p_s->resize(24,0)" << std::endl ;
        p_s->resize(24, 0) ;
        printInfo() ;
        
        
        // p_s->get_allocator().finalize() ;
        /*
        std::cout << "INFO: " << rank_ << ": std::move(p_s) " << std::endl ;
        *p2_s = std::move(*p_s) ;
        
        std::cout << "INFO: " << rank_ << ": p2_s->get_allocator " << std::endl ;
        auto tMalloc = p2_s.get_allocator() ;
        s_alloc_p2 = tMalloc ;
        printInfo() ;
        */
        
    }
    int rank_ ;

    // Mallocator<int> m_alloc_p(std::string("PRESSURE"), rank) ; // Mallocator<int>() ;
    // Mallocator<int> * m_alloc_p ;
    std::vector<int, Mallocator<int> > * p_s ;

private:
    
} ; // class UseVect

template<class T, class U>
bool operator==(const Mallocator <T>&, const Mallocator <U>&) { return true; }
template<class T, class U>
bool operator!=(const Mallocator <T>&, const Mallocator <U>&) { return false; }



int main(int argc, char** argv)

{
    MPI_Init(&argc,&argv) ;
    damaris_initialize("allocate.xml", MPI_COMM_WORLD) ;
    int is_client;
    int size;
    int rank;
    int err = damaris_start(&is_client);

    if((err == DAMARIS_OK || err == DAMARIS_NO_SERVER) && is_client) 
    {
        MPI_Comm comm;
        damaris_client_comm_get(&comm);
        
        MPI_Comm_rank(comm , &rank);
        MPI_Comm_size(comm , &size);
        
        err = damaris_parameter_set("size" , &size, sizeof(int)) ;
        if(err != DAMARIS_OK ){
           std::cerr << " Daamris error: " <<  damaris_error_string(err) << std::endl ;
        }
        UseVect * uv = new UseVect (rank) ;
        std::cout << "INFO: UseVect has been defined" << std::endl ;
        uv->TestAlloc() ;
       
       
        std::cout << "INFO: UseVect will be deleted" << std::endl ;
        delete uv ;
        std::cout << "INFO: UseVect has been deleted" << std::endl ;
       // std::vector<double, Mallocator<double> > t(10, myalloc_t);
       // t.push_back(373.15);
       std::cout << "INFO: Calling: damaris_end_iteration() " << std::endl ;
       damaris_end_iteration() ;
       std::cout << "INFO: Calling: damaris_stop() "<< std::endl ;
       damaris_stop( );
    }
    
    err = damaris_finalize();
    if(err != DAMARIS_OK ){
       std::cerr << " Daamris error: " <<  damaris_error_string(err) << std::endl ;
    }
    MPI_Finalize();

}
