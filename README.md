# Using Damaris API for C++ Memory Allocation

## Introduction

The Damaris library provides an API to allow an application to allocate memory directly from the 
managed shared memory region that is used to aggregate data from multiple MPI processes from within 
a compute node.

This project tests the use of the Damaris shared memory allocation routines applied to use as a 
C++ standard library container memory allocator, specifically for use with std::vector<>, however, 
potentially for any C++ standard library data container that accepts a user defined allocator.

## Current implementation

The Mallocator class is an simple test case of a double buffered std lib compliant memory allocator.
The class attempts to use two Damaris 'blocks' as alternating buffers as an allocator for a 
std::vector<> object.



## Issues found
Unfortunately, the Damaris shared memory allocator (damaris_alloc_block() API) is not dynamic 
enough to deal with the requirements of a std::vector<> allocator (class Mallocator in the 
example code ). Its use runs into issues as within a Damaris "iteration" only a single memory 
region size can be assigned to a block (using damaris_paramater_set() API). This limits the 
number of resizes a std::vector<> object can undertake. This issue is negated if the total 
memory size needed for the container is known up-front for an iteration. In this case, the 
allocator will work as desired and the double buffering is not required.
  
The double buffering is needed, as when a container resizes, it allocates a new memory area 
and copies the original data over to the new region. Then, typically it removes the old region, 
although this behavior is changed in the Mallocator so as the original memory area is retained.

Currently, the double buffering using two Damaris blocks causes issues as only the final active 
memory block will contain the desired data and the other block will contain the previous copy. 
Damaris dos not provide an API to remove an unwanted block or specify that it should not be saved.

## To compile 
Damaris must be installed in the case below it is installed at ~/local/lib. 
```
# Path to Damaris library needs to be added
export LD_LIBRARY_PATH=~/local/lib:$LD_LIBRARY_PATH

mpicxx -I ~/local/include -L ~/local/lib  -o test.exe main.cpp -ldamaris
```

## To run
the executable needs to have alloacte.xml file in the same directory.
  
```
./test.exe
```

## Conclusion
An addition of Damaris API functions that can resize a current block within the current iteration 
and also an API that deletes or nullifies a block that is not required could adequately allow 
for a Damaris capable C++ standard library memory allocator. Having an allocator that uses the 
Damaris APIwcould allow C++ programs to rapidly add Damaris capabilities.    
